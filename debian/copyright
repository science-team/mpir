Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mpir
Source: http://mpir.org/
Files-Excluded: doc/*.texi doc/mpir.info* yasm

Files: *
Copyright: 1991, 1996, 1999, 2000 Free Software Foundation, Inc.
	   2008, 2009 William Hart <goodwillhart@googlemail.com>
License: LGPL-2.1+

Files: mpn/generic/mul_n.c mpn/generic/addadd_n.c mpn/generic/sumdiff_n.c
 mpn/generic/addsub_n.c mpn/generic/toom_eval_pm1.c mpn/generic/subadd_n.c
Copyright: 2011-2012 The Code Cavern
License: LGPL-2.1+

Files: mpz/fac_ui.c
Copyright: 2009 Robert Gerbicz
License: LGPL-2.1+

Files: mpz/next_likely_prime.c mpz/get_ux.c mpz/millerrabin.c mpz/iset_ux.c
 mpz/set_ux.c mpz/miller_rabin.c mpz/set_sx.c mpz/get_sx.c mpz/iset_sx.c
 tests/mpz/t-set_ux.c tests/mpz/t-get_ux.c tests/mpz/t-next_likely_prime.c
 mpf/set_str.c
Copyright: 2009, 2011 Brian Gladman <brg@gladman.plus.com>
License: LGPL-2.1+

Files: mpz/perfpow.c mpz/next_likely_prime.c mpz/nthroot.c mpz/trial_division.c
 mpz/likely_prime_p.c mpz/probable_prime_p.c mpz/nextprime.c
 mpn/x86_64/fat/submul_1.c mpn/x86_64/fat/mul_1.c mpn/x86_64/fat/add_n.c
 mpn/x86_64/fat/sub_n.c mpn/x86_64/fat/addmul_1.c mpn/x86_64/fat/lshift.c
 mpn/x86_64/fat/rshift.c mpn/generic/urandomm.c
 mpn/generic/rsh_divrem_hensel_qr_1_2.c mpn/generic/toom3_mul.c
 mpn/generic/mul_n.c mpn/generic/rsh_divrem_hensel_qr_1_1.c
 mpn/generic/divrem_hensel_qr_1_1.c mpn/generic/divrem_hensel_qr_1_2.c
 mpn/generic/divexact_by3c.c mpn/generic/mulhigh_n.c
 mpn/generic/divrem_hensel_r_1.c mpn/generic/mulmod_2expp1_basecase.c
 mpn/generic/rrandom.c mpn/generic/randomb.c mpn/generic/neg_n.c
 mpn/generic/rsh_divrem_hensel_qr_1.c mpn/generic/toom3_mul_n.c
 mpn/generic/divrem_hensel_rsh_qr_1.c mpn/generic/mulmod_2expm1.c
 mpn/generic/rootrem_basecase.c mpn/generic/mullow_n.c
 mpn/generic/divexact_byfobm1.c mpn/generic/mullow_basecase.c
 mpn/generic/urandomb.c mpn/generic/divrem_hensel_qr_1.c
 mpn/x86_64w/fat/submul_1.c mpn/x86_64w/fat/mul_1.c mpn/x86_64w/fat/add_n.c
 mpn/x86_64w/fat/sub_n.c mpn/x86_64w/fat/addmul_1.c mpn/x86_64w/fat/lshift.c
 mpn/x86_64w/fat/rshift.c cpuid.c tests/mpz/t-perfpow.c
 tests/mpz/t-trial_division.c tests/mpn/t-mullow_basecase.c
 tests/mpn/t-mullowhigh.c tests/mpn/t-redc_1.c tests/mpn/t-lorrshift1.c
 tests/mpn/t-mulmod_2expp1.c tests/mpn/t-mulmod_2expm1.c tests/mpf/t-eq.c
 mpf/eq.c mpf/rrandomb.c
Copyright: 2008-2011 Jason Moxham <jason@njkfrudils.plus.com>
License: LGPL-2.1+

Files: mpz/likely_prime_p.c
Copyright: 2008 Peter Shrimpton
License: LGPL-2.1+

Files: cpuid.c
Copyright: 2010 Gonzalo Tornaria
License: LGPL-2.1+

Files: mpn/generic/invert.c tests/mpn/t-invert.c
Copyright: 2009 Paul Zimmermann  <zimmermann@inria.inria.fr>
License: LGPL-2.1+

Files: errno.c gmp-impl.h mpf/clears.c mpf/inits.c mpirxx.h mpn/generic/and_n.c
 mpn/generic/andn_n.c mpn/generic/com_n.c mpn/generic/dc_bdiv_q.c
 mpn/generic/dc_bdiv_qr.c mpn/generic/dc_bdiv_qr_n.c mpn/generic/dc_divappr_q.c
 mpn/generic/dc_div_q.c mpn/generic/dc_div_qr.c mpn/generic/dc_div_qr_n.c
 mpn/generic/divexact.c mpn/generic/gcd_1.c mpn/generic/gcd.c
 mpn/generic/gcdext_1.c mpn/generic/gcdext.c mpn/generic/gcdext_lehmer.c
 mpn/generic/gcdext_subdiv_step.c mpn/generic/gcd_lehmer.c
 mpn/generic/gcd_subdiv_step.c mpn/generic/get_str.c mpn/generic/hamdist.c
 mpn/generic/hgcd2.c mpn/generic/hgcd.c mpn/generic/inv_divappr_q.c
 mpn/generic/inv_div_q.c mpn/generic/inv_div_qr.c mpn/generic/ior_n.c
 mpn/generic/iorn_n.c mpn/generic/matrix22_mul.c mpn/generic/nand_n.c
 mpn/generic/nior_n.c mpn/generic/popcount.c mpn/generic/random2.c
 mpn/generic/redc_2.c mpn/generic/rootrem.c mpn/generic/sb_bdiv_q.c
 mpn/generic/sb_bdiv_qr.c mpn/generic/sb_divappr_q.c mpn/generic/sb_div_q.c
 mpn/generic/sb_div_qr.c mpn/generic/scan0.c mpn/generic/scan1.c
 mpn/generic/set_str.c mpn/generic/tdiv_q.c mpn/generic/tdiv_qr.c
 mpn/generic/toom8h_mul.c mpn/generic/toom8_sqr_n.c
 mpn/generic/toom_couple_handling.c mpn/generic/toom_eval_dgr3_pm1.c
 mpn/generic/toom_eval_dgr3_pm2.c mpn/generic/toom_eval_pm2.c
 mpn/generic/toom_eval_pm2exp.c mpn/generic/toom_eval_pm2rexp.c
 mpn/generic/toom_interpolate_16pts.c mpn/generic/xnor_n.c mpn/generic/xor_n.c
 mpn/generic/zero.c mpn/mips32/longlong_inc.h mpn/mips64/longlong_inc.h
 mpq/clears.c mpq/inits.c mpq/md_2exp.c mpz/clears.c mpz/cong_2exp.c
 mpz/divexact.c mpz/divis_2exp.c mpz/hamdist.c mpz/inits.c mpz/root.c
 mpz/rootrem.c mpz/rrandomb.c mpz/scan0.c mpz/scan1.c mpz/tstbit.c
 tests/mpn/t-hgcd.c tests/mpn/t-matrix22.c tune/set_strb.c tune/set_strp.c
 tune/set_strs.c
Copyright: 1991-2012 Free Software Foundation, Inc.
License: LGPL-3+

Files: mpz/rootrem.c
Copyright: 2010 Dr B R Gladman <brg@gladman.plus.com>
License: LGPL-3+

Files: mpn/generic/tdiv_q.c mpn/generic/dc_bdiv_qr.c mpn/generic/sb_bdiv_qr.c
 mpn/generic/inv_div_qr.c mpn/generic/dc_bdiv_qr_n.c mpn/generic/sb_div_qr.c
 mpn/generic/inv_div_q.c mpn/generic/dc_divappr_q.c mpn/generic/inv_divappr_q.c
 mpn/generic/sb_divappr_q.c mpn/generic/sb_bdiv_q.c mpn/generic/tdiv_qr.c
 mpn/generic/dc_div_q.c mpn/generic/dc_bdiv_q.c mpn/generic/sb_div_q.c
 mpn/generic/dc_div_qr.c mpn/generic/divexact.c mpn/generic/dc_div_qr_n.c
 mpirxx.h gmp-impl.h
Copyright: 2009-2010 William Hart <goodwillhart@googlemail.com>
License: LGPL-3+

Files: mpn/generic/sb_bdiv_q.c
Copyright: 2009 David Harvey <d.harvey@unsw.edu.au>
License: LGPL-3+

Files: mpn/generic/toom_eval_dgr3_pm1.c
Copyright: 2011 Jason Moxham <jason@njkfrudils.plus.com>
License: LGPL-3+

Files: fft/adjust.c fft/adjust_sqrt2.c fft/butterfly_lshB.c
 fft/butterfly_rshB.c fft/combine_bits.c fft/div_2expmod_2expp1.c
 fft/fermat_to_mpz.c fft/fft_mfa_trunc_sqrt2.c fft/fft_mfa_trunc_sqrt2_inner.c
 fft/fft_negacylic.c fft/fft_radix2.c fft/fft_trunc.c fft/fft_trunc_sqrt2.c
 fft/ifft_mfa_trunc_sqrt2.c fft/ifft_negacyclic.c fft/ifft_radix2.c
 fft/ifft_trunc.c fft/ifft_trunc_sqrt2.c fft/mul_2expmod_2expp1.c
 fft/mul_fft_main.c fft/mul_mfa_trunc_sqrt2.c fft/mulmod_2expp1.c
 fft/mul_trunc_sqrt2.c fft/normmod_2expp1.c fft/split_bits.c
 fft/tune/tune-fft.c mpn/generic/dc_bdiv_q_n.c mpn/generic/dc_divappr_q_n.c
 mpn/generic/mulmod_bexpp1.c tests/fft/t-adjust.c tests/fft/t-adjust_sqrt2.c
 tests/fft/t-butterfly.c tests/fft/t-butterfly_lshB.c
 tests/fft/t-butterfly_rshB.c tests/fft/t-butterfly_sqrt2.c
 tests/fft/t-butterfly_twiddle.c tests/fft/t-div_2expmod_2expp1.c
 tests/fft/t-fft_ifft_mfa_trunc_sqrt2.c tests/fft/t-fft_ifft_negacyclic.c
 tests/fft/t-fft_ifft_radix2.c tests/fft/t-fft_ifft_trunc.c
 tests/fft/t-fft_ifft_trunc_sqrt2.c tests/fft/t-mul_2expmod_2expp1.c
 tests/fft/t-mul_fft_main.c tests/fft/t-mul_mfa_trunc_sqrt2.c
 tests/fft/t-mulmod_2expp1.c tests/fft/t-mul_trunc_sqrt2.c
 tests/fft/t-normmod_2expp1.c tests/fft/t-split_combine_bits.c
Copyright: 2009, 2011 William Hart <goodwillhart@googlemail.com>
License: BSD-2-clause

Files: mpn/generic/add_err1_n.c mpn/generic/add_err2_n.c
 mpn/generic/dc_bdiv_q_n.c mpn/generic/dc_divappr_q_n.c
 mpn/generic/mulmid_basecase.c mpn/generic/mulmid.c mpn/generic/mulmid_n.c
 mpn/generic/sub_err1_n.c mpn/generic/sub_err2_n.c mpn/generic/toom42_mulmid.c
 tests/mpn/t-mulmid.c tests/refmpn.c
Copyright: 2009, 2010 David Harvey <d.harvey@unsw.edu.au>
License: BSD-2-clause

Files: fft/revbin.c
Copyright: 2009 William Hart <goodwillhart@googlemail.com>
License: GPL-2+

Files: debian/*
Copyright: 2013 Felix Salfelder <felix@salfelder.org>
	   2015 Doug Torrance <dtorrance@monmouthcollege.edu>
License: LGPL-2.1+

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
